import { GamesActionTypes, ISortGamesAction } from '../../actions/gamesActions'
import gamesReducer, { initialGamesState } from '../../reducers/gamesReducer'

describe('INITIAL_STATE', () => {
  test('should return new correct intitial state with default values when passing initialState equals undefined and action a dummy action', () => {
    const action = { type: 'DUMMY_ACTION', payload: [] }

    // @ts-ignore => avoid IDE warning that dummy_action does not match action type
    expect(gamesReducer(undefined, action)).toEqual(initialGamesState)
  })
})

// snapshot test
describe('SORT_GAMES', () => {
  test('returns the correct state', () => {
    const action: ISortGamesAction = {
      type: GamesActionTypes.SORT_GAMES,
      payload: { sortCriteria: 'popular' }
    }

    expect(gamesReducer(undefined, action)).toMatchSnapshot()
  })
})
