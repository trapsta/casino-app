import { GamesActionTypes, loadGames } from '../../actions/gamesActions'

describe('game actions', () => {
  it('loadGames action creator should create an action to load games', () => {
    const expectedAction = {
      type: GamesActionTypes.LOAD_GAMES
    }

    expect(loadGames()).toEqual(expectedAction)
  })
})
