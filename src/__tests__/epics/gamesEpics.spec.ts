import { StateObservable, ActionsObservable } from 'redux-observable'
import { Subject } from 'rxjs'
import { GamesAction, GamesActionTypes } from '../../actions/gamesActions'
import loadGamesEpic from '../../epics/gamesEpics'
import { initialState, IState } from '../../reducers'

describe('loadGamesEpic', () => {
  it('LOAD_GAMES dispatches LOADED_GAMES after successfuly fetching games from the GamesApiService', () => {
    const state$ = new StateObservable<IState>(new Subject(), initialState)
    const action$ = ActionsObservable.of({
      type: GamesActionTypes.LOAD_GAMES
    }) as ActionsObservable<GamesAction>

    loadGamesEpic(action$, state$, {}).subscribe((outputActions) => {
      expect(outputActions).toEqual([GamesActionTypes.LOADED_GAMES])
    })
  })
})
