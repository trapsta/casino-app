import { createSelector } from 'reselect'
import { ICategoryItem, IGameItem, ISortCriteria } from '../models'
import { IState } from '../reducers'
import filter from 'lodash/filter'
import { findIndex, sortBy } from 'lodash'

const gamesSelector = (state: IState) => state.games.games
const sortCriteriaSelector = (state: IState) => state.games.sortCriteria
const filterSelector = (state: IState) => state.games.filter

export const filteredGamesSelector = createSelector(
  gamesSelector,
  filterSelector,
  sortCriteriaSelector,
  (games: IGameItem[], filterCategory: ICategoryItem, sort: ISortCriteria) => {
    return sortGames(
      filter(games, { categories: [{ categoryId: filterCategory.id }] }),
      sort,
      filterCategory
    )
  }
)

const sortGames = (
  games: IGameItem[],
  sort: ISortCriteria,
  selectedCategory?: ICategoryItem
): IGameItem[] => {
  if (sort === 'alphabetical') {
    return [...games].sort((a, b) => (a.name < b.name ? -1 : 1))
  }
  if (sort === 'popular') {
    return sortBy(games, (item) => {
      // we first find the specified order for this specific category
      const categorySorting = findIndex(
        item.categories,
        (cat) => cat.categoryId === selectedCategory?.id
      )
      return item.categories[categorySorting].orderNumber
    })
  }
  return games
}
