import Container from '@material-ui/core/Container'
import makeStyles from '@material-ui/core/styles/makeStyles'
import React, { ReactNode } from 'react'

import './style.scss'

interface Props {
  children: ReactNode
}

export default function BasePage({ children }: Props) {
  const classes = useStyles()

  return (
    <Container className={classes.container}>
      <div className={'app-root-wrapper'}>{children}</div>
    </Container>
  )
}

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.primary.light
  }
}))
