import React, { useState } from 'react'
import { fade, makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Toolbar from '@material-ui/core/Toolbar'
import AppBar from '@material-ui/core/AppBar'
import IconButton from '@material-ui/core/IconButton'
import Badge from '@material-ui/core/Badge'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search'
import MoreIcon from '@material-ui/icons/MoreVert'
import AppsIcon from '@material-ui/icons/Apps'
import Button from '@material-ui/core/Button'
import SearchInput from '../SearchInput'
import Container from '@material-ui/core/Container'
import { ISortCriteria } from '../../models'

import './style.scss'
import { isMobile } from '../../config'

interface Props {
  changeSortCriteria: (sort: 'alphabetical' | 'popular') => void
  changeLayout: (layout: 'list' | 'grid') => void
  gridLayout: boolean
  sortCriteria: ISortCriteria
  toggleCategories: () => void
}

const Navigation = ({
  changeLayout,
  sortCriteria,
  gridLayout,
  changeSortCriteria,
  toggleCategories
}: Props) => {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null)

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)

  const classes = useStyles()

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const handleMobileMenuOpen = (event: any) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }

  const mobileMenuId = 'primary-search-menu-mobile'
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <Button
          variant="text"
          disableRipple
          color={sortCriteria === 'popular' ? 'primary' : 'inherit'}
          onClick={() => changeSortCriteria('popular')}
          className={clsx(
            'nav-item-btn',
            sortCriteria === 'popular' ? classes.active : undefined
          )}
        >
          Popularity
        </Button>
      </MenuItem>
      <MenuItem>
        <Button
          variant="text"
          disableRipple
          color={sortCriteria === 'alphabetical' ? 'primary' : 'inherit'}
          onClick={() => changeSortCriteria('alphabetical')}
          className={clsx(
            'nav-item-btn',
            sortCriteria === 'alphabetical' ? classes.active : undefined
          )}
        >
          Alphabetically
        </Button>
      </MenuItem>
      <MenuItem onClick={() => changeLayout('grid')}>
        <IconButton
          aria-label="show grid view"
          color="inherit"
          onClick={() => changeLayout('grid')}
          className={clsx(gridLayout ? classes.active : undefined)}
        >
          <Badge color="secondary">
            <AppsIcon />
          </Badge>
        </IconButton>
      </MenuItem>
      <MenuItem onClick={() => changeLayout('list')}>
        <IconButton
          aria-label="show list view"
          color="inherit"
          onClick={() => changeLayout('list')}
          className={clsx(!gridLayout ? classes.active : undefined)}
        >
          <Badge color="secondary">
            <MenuIcon />
          </Badge>
        </IconButton>
      </MenuItem>
    </Menu>
  )

  return (
    <div className="navigation-wrapper">
      <AppBar
        position="fixed"
        color="transparent"
        className={classes.appNavBar}
      >
        <Container>
          <Toolbar>
            {isMobile() && (
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
                onClick={toggleCategories}
              >
                <MenuIcon />
              </IconButton>
            )}
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <SearchInput />
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <Button
                variant="text"
                disableRipple
                color={sortCriteria === 'popular' ? 'primary' : 'inherit'}
                onClick={() => changeSortCriteria('popular')}
                className={clsx(
                  'nav-item-btn',
                  sortCriteria === 'popular' ? classes.active : undefined
                )}
              >
                Popularity
              </Button>
              <Button
                variant="text"
                disableRipple
                color={sortCriteria === 'alphabetical' ? 'primary' : 'inherit'}
                onClick={() => changeSortCriteria('alphabetical')}
                className={clsx(
                  'nav-item-btn',
                  sortCriteria === 'alphabetical' ? classes.active : undefined
                )}
              >
                Alphabetically
              </Button>
              <IconButton
                aria-label="show grid view"
                color="inherit"
                onClick={() => changeLayout('grid')}
                className={clsx(gridLayout ? classes.active : undefined)}
              >
                <Badge color="secondary">
                  <AppsIcon />
                </Badge>
              </IconButton>
              <IconButton
                aria-label="show list view"
                color="inherit"
                onClick={() => changeLayout('list')}
                className={clsx(!gridLayout ? classes.active : undefined)}
              >
                <Badge color="secondary">
                  <MenuIcon />
                </Badge>
              </IconButton>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="inherit"
              >
                <MoreIcon />
              </IconButton>
            </div>
          </Toolbar>
        </Container>
      </AppBar>
      {renderMobileMenu}
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  appNavBar: {
    backgroundColor: '#fff',
    color: theme.palette.primary.dark
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.primary.light,
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 'auto'
    }
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.primary.dark
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    fontWeight: 'bold',
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch'
    }
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  active: {
    color: theme.palette.primary.main
  }
}))

export default Navigation
