import React, { Fragment } from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link'
import { ICategoryItem } from '../../models'
import { isMobile } from '../../config'

interface Props {
  categories: ICategoryItem[]
  filter: ICategoryItem
  handleFilter: (category: ICategoryItem) => void
}

const SideBar = ({ categories, filter, handleFilter }: Props) => {
  const classes = useStyles()

  const itemLink = (item: ICategoryItem, secondary?: boolean) => (
    <Link
      display="block"
      underline="none"
      color={filter.id === item.id ? 'textPrimary' : 'textSecondary'}
      onClick={() => handleFilter(item)}
      className={clsx(
        classes.item,
        { [classes.secondaryItem]: secondary },
        filter.id === item.id ? classes.active : undefined
      )}
    >
      <span>{item.displayName}</span>
    </Link>
  )

  return (
    <nav
      className={isMobile() ? classes.mobileRoot : classes.root}
      aria-label={'filter games by categories'}
    >
      {categories.length > 0 ? (
        <Fragment>
          <Typography gutterBottom className={classes.contents}>
            categories
          </Typography>
          <Typography component="ul" className={classes.ul}>
            {categories.map((category) => (
              <li key={category.id}>{itemLink(category)}</li>
            ))}
          </Typography>
        </Fragment>
      ) : null}
    </nav>
  )
}

export default SideBar

const useStyles = makeStyles((theme) => ({
  root: {
    top: 70,
    // Fix IE11 position sticky issue.
    marginTop: 70,
    width: 175,
    flexShrink: 0,
    position: 'sticky',
    height: 'calc(100vh - 70px)',
    overflowY: 'auto',
    padding: theme.spacing(2, 2, 2, 0),
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  mobileRoot: {
    padding: '.5rem',
    paddingLeft: 0
  },
  contents: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    paddingLeft: theme.spacing(1),
    textTransform: 'uppercase',
    color: theme.palette.primary.dark
  },
  ul: {
    padding: 0,
    margin: 0,
    listStyle: 'none'
  },
  item: {
    cursor: 'pointer',
    fontSize: '1rem',
    margin: '.5rem 0',
    padding: theme.spacing(0.5, 0, 0.5, '5px'),
    borderLeft: `3px solid transparent`,
    boxSizing: 'border-box',
    '&:hover': {
      borderLeftColor: theme.palette.grey[200]
    },
    '&$active,&:active': {
      borderLeftColor: theme.palette.grey[300]
    }
  },
  secondaryItem: {
    paddingLeft: theme.spacing(2.5)
  },
  active: {
    color: theme.palette.primary.main
  }
}))
