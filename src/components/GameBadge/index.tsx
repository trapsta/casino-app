import React from 'react'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

interface Props {
  color?: string
  text: string
  title?: string
  topRight?: boolean
  inline?: boolean
}

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'absolute',
      right: 0,
      bottom: '3.9rem',
      backgroundColor: '#474747',
      padding: '.25rem',
      color: '#fff',
      fontWeight: 'bold',
      fontSize: '.875rem',
      boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2)'
    },
    topRight: {
      top: '.5rem',
      right: '.5rem',
      bottom: 'unset'
    },
    badgetag: {
      padding: '0 .35rem',
      display: 'inline-block',
      marginRight: '.5rem'
    },
    badgeTitle: {
      margin: 0,
      fontSize: '.75rem'
    },
    inlineBadge: {
      position: 'relative',
      top: 'unset',
      right: 'unset',
      bottom: 'unset',
      marginLeft: '.5rem',
      padding: '0 .25rem'
    }
  })
)

const GameBadge = ({
  color = '#36b14f',
  text,
  title,
  topRight = false,
  inline = false
}: Props) => {
  const classes = useStyles()

  return (
    <div
      className={clsx(
        classes.root,
        topRight ? classes.topRight : undefined,
        inline ? classes.inlineBadge : undefined
      )}
      style={{ backgroundColor: !title ? color : undefined }}
    >
      <span
        className={classes.badgetag}
        style={{ backgroundColor: color, marginRight: !title ? 0 : undefined }}
      >
        {text}
      </span>
      {title && <span className={classes.badgeTitle}>{title}</span>}
    </div>
  )
}

export default GameBadge
