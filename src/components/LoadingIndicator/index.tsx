import React from 'react'
import clsx from 'clsx'

import './style.scss'

interface Props {
  isWhite?: boolean
  className?: string
  transparent?: boolean
}

const LoadingIndicator = ({
  isWhite,
  className = 'loading-dots-animation',
  transparent
}: Props) => {
  const CSS = {
    LoadingWrapper: 'loading-wrapper',
    LoadingDots: 'loading-dots',
    LoadingDotsWhite: 'loading-dots-white',
    LoadingDot: 'loading-dot',
    loadingBlinks: 'loading-blinks',
    LoadingDot2: 'loading-dot-2',
    LoadingDot3: 'loading-dot-3',
    Transparent: 'transparent-loading-dots'
  }

  return (
    <div className={clsx(className, CSS.LoadingWrapper)}>
      <div
        className={clsx(CSS.LoadingDots, {
          [CSS.LoadingDotsWhite]: isWhite,
          [CSS.Transparent]: transparent,
          [className]: className
        })}
      >
        <span className={CSS.LoadingDot}>·</span>
        <span className={`${CSS.LoadingDot} ${CSS.LoadingDot2}`}>·</span>
        <span className={`${CSS.LoadingDot} ${CSS.LoadingDot3}`}>·</span>
      </div>
    </div>
  )
}

export default LoadingIndicator
