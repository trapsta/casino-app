import React, { Fragment, useEffect, useState } from 'react'
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles
} from '@material-ui/core/styles'
import { IGameItem, ApiStatus, ICategoryItem, ISortCriteria } from '../models'
import Alert from '@material-ui/lab/Alert'
import BasePage from './BasePage'
import SideBar from './SideBar'
import Navigation from './Navigation'
import { ISearchState } from '../reducers/searchReducer'
import LoadingIndicator from './LoadingIndicator'
import CardsGrid from './CardsGrid'
import AlertTitle from '@material-ui/lab/AlertTitle/AlertTitle'
import Drawer from '@material-ui/core/Drawer'
import clsx from 'clsx'

import './style.scss'
import { isMobile } from '../config'
import { debounce } from 'lodash'

export interface IAppStateProps {
  loadingStatus: ApiStatus
  games: IGameItem[]
  categories: ICategoryItem[]
  searchState: ISearchState
  sortCriteria: ISortCriteria
  filter: ICategoryItem
}

export interface IAppDispatchProps {
  loadGames: () => void
  loadCategories: () => void
  sortGames: (sortCriteria: ISortCriteria) => void
  filterGames: (category: ICategoryItem) => void
  loadingGames: () => void
}

type AppProps = IAppStateProps & IAppDispatchProps & WithStyles<typeof styles>

const App = ({
  loadGames,
  loadCategories,
  games,
  categories,
  loadingStatus,
  classes,
  searchState,
  sortCriteria,
  sortGames,
  filterGames,
  filter
}: AppProps) => {
  const { search } = searchState
  const [gridLayout, setGridLayout] = useState<boolean>(true)
  const [showCategories, setShowCategories] = useState<boolean>(false)
  const [, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth
  })

  useEffect(() => {
    // Rerender view on window resize
    const debouncedHandleResize = debounce(function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      })
    }, 1000)

    window.addEventListener('resize', debouncedHandleResize)

    return () => {
      // clean up event listener to avoid memory leaks
      window.removeEventListener('resize', debouncedHandleResize)
    }
  }, [])

  useEffect(() => {
    if (loadGames) {
      loadGames()
    }

    if (loadCategories) {
      loadCategories()
    }
  }, [loadGames, loadCategories])

  const changeLayout = (layout: 'grid' | 'list') => {
    setGridLayout(layout === 'grid' ? true : false)
  }

  const changeSortCriteria = (sortOrder: ISortCriteria) => {
    sortGames(sortOrder)
  }

  const filterGamesByCategory = (category: ICategoryItem) => {
    filterGames(category)
  }

  const toggleDrawer = () => (
    event: React.KeyboardEvent | React.MouseEvent
  ) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return
    }

    setShowCategories(!showCategories)
  }

  return (
    <BasePage>
      <Navigation
        changeSortCriteria={changeSortCriteria}
        changeLayout={changeLayout}
        sortCriteria={sortCriteria}
        gridLayout={gridLayout}
        toggleCategories={() => setShowCategories(!showCategories)}
      />
      {(!search || search === '') && (
        <Fragment>
          {isMobile() ? (
            <Drawer
              anchor={'left'}
              open={showCategories}
              onClose={() => setShowCategories(false)}
            >
              <div
                className={clsx(classes.list)}
                role="presentation"
                onClick={toggleDrawer()}
                onKeyDown={toggleDrawer()}
              >
                <SideBar
                  categories={categories}
                  filter={filter}
                  handleFilter={filterGamesByCategory}
                />
              </div>
            </Drawer>
          ) : (
            <SideBar
              categories={categories}
              filter={filter}
              handleFilter={filterGamesByCategory}
            />
          )}
        </Fragment>
      )}
      <div className="main-content-wrapper">
        <main id="main-content">
          <div className={classes.wrap}>
            <div className={classes.content}>
              <div>
                {loadingStatus === ApiStatus.LOADING && <LoadingIndicator />}

                {loadingStatus === ApiStatus.FAILED && (
                  <Alert severity="error" variant="filled">
                    <AlertTitle>Error</AlertTitle>
                    Error! Failed to load games
                  </Alert>
                )}

                {((search && search !== '' && searchState.games.length < 1) ||
                  (!search && search === '' && games.length < 1)) && (
                  <Alert severity="warning">
                    <AlertTitle>Oops!</AlertTitle>
                    No games found for{' '}
                    <b>
                      {search && search !== '' ? search : filter.displayName}
                    </b>
                  </Alert>
                )}

                <CardsGrid
                  games={search && search !== '' ? searchState.games : games}
                  spacing={gridLayout ? 4 : 0}
                  gridLayout={gridLayout}
                />
              </div>
            </div>
          </div>
        </main>
      </div>
    </BasePage>
  )
}

const styles = (theme: Theme) =>
  createStyles({
    wrap: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      textAlign: 'center',
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(8),
      [theme.breakpoints.up('md')]: {
        paddingTop: theme.spacing(16),
        paddingBottom: theme.spacing(16),
        flexDirection: 'row',
        alignItems: 'flex-start',
        textAlign: 'left'
      }
    },
    content: {
      width: '100%'
    },
    addButton: {
      marginTop: theme.spacing(1)
    },
    divider: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2)
    },
    gameItem: {
      padding: 20,
      marginTop: 20,
      marginBottom: 20
    },
    listItem: {
      fontSize: '1rem',
      textTransform: 'none'
    },
    list: {
      width: 250
    },
    fullList: {
      width: 'auto'
    }
  })

export default withStyles(styles)(App)
