import React, { ChangeEvent } from 'react'
import { useDispatch } from 'react-redux'
import { changeSearchTerm } from '../../actions/searchActions'
import InputBase from '@material-ui/core/InputBase'
import { makeStyles } from '@material-ui/core/styles'

const SearchInput = () => {
  const classes = useStyles()

  const dispatch = useDispatch()

  const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    dispatch(changeSearchTerm(value))
  }

  return (
    <>
      <InputBase
        placeholder="Find a game"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        inputProps={{ 'aria-label': 'Find a game' }}
        onChange={handleSearch}
        type="search"
      />
    </>
  )
}

export default SearchInput

const useStyles = makeStyles((theme) => ({
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    fontWeight: 'bold',
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch'
    }
  }
}))
