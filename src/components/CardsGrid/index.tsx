import React, { Fragment } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import GameCard from '../GameCard'
import { IGameItem } from '../../models'
import Button from '@material-ui/core/Button'
import GameBadge from '../GameBadge'

interface Props {
  games: IGameItem[]
  gridLayout?: boolean
  spacing?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      height: 140,
      width: 100
    },
    control: {
      padding: theme.spacing(2)
    },
    listItem: {
      fontSize: '1rem',
      textTransform: 'none',
      color: theme.palette.primary.dark
    }
  })
)

export default function CardsGrid({ games, gridLayout, spacing = 2 }: Props) {
  const classes = useStyles()

  return (
    <Grid container spacing={spacing}>
      {games.map((game) => (
        <Fragment key={game.gameId}>
          {gridLayout ? (
            <Grid key={game.gameId} item xs={12} sm={6} md={4} lg={4} xl={3}>
              <GameCard game={game} />
            </Grid>
          ) : (
            <Grid key={game.gameId} item xs={12} sm={6}>
              <Button
                variant="text"
                className={classes.listItem}
                disableElevation
                disableFocusRipple
              >
                {game.name}
                {game.isNew && <GameBadge color="#f4c21f" text="New" inline />}
                {game.isHot && <GameBadge color="#d61d1e" text="Hot" inline />}
                {game.isLive && (
                  <GameBadge color="#2587c0" text="Live" inline />
                )}
                {game.jackpot && <GameBadge text="J" inline />}
              </Button>
            </Grid>
          )}
        </Fragment>
      ))}
    </Grid>
  )
}
