import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import { IGameItem } from '../../models'
import { imgCdn } from '../../config'
import GameBadge from '../GameBadge'

interface Props {
  game: IGameItem
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    margin: '0 auto',
    cursor: 'pointer',
    borderRadius: '0'
  },
  media: {
    width: '100%',
    height: 0,
    paddingBottom: '75%',
    backgroundColor: 'rgba(0, 0, 0, 0.08)'
  },
  cardTitle: {
    fontSize: '1rem',
    color: theme.palette.primary.dark,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  }
}))

const GameCard = ({ game }: Props) => {
  const classes = useStyles()

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={imgCdn(game.gameId)}
          title={game.name}
        >
          {game.isNew && <GameBadge color="#f4c21f" text="New" topRight />}
          {game.isHot && <GameBadge color="#d61d1e" text="Hot" topRight />}
          {game.isLive && <GameBadge color="#2587c0" text="Live" topRight />}
          {game.jackpot && (
            <GameBadge title={game.jackpot.currencyFormat} text="J" />
          )}
        </CardMedia>
        <CardContent>
          <Typography
            gutterBottom
            variant="h6"
            component="h2"
            className={classes.cardTitle}
          >
            {game.name}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default GameCard
