import App, { IAppStateProps, IAppDispatchProps } from '../components/App'
import { IState } from '../reducers'
import {
  loadGames,
  sortGames,
  filterGames,
  loadingGames
} from '../actions/gamesActions'
import { loadCategories } from '../actions/categoriesActions'
import { connect } from 'react-redux'
import { filteredGamesSelector } from '../selectors'

function mapStateToDispatch(state: IState): IAppStateProps {
  return {
    games: filteredGamesSelector(state),
    loadingStatus: state.games.loadingStatus,
    categories: state.categories.categories,
    searchState: state.search,
    sortCriteria: state.games.sortCriteria,
    filter: state.games.filter
  }
}

const mapDispatchToProps: IAppDispatchProps = {
  loadGames,
  loadCategories,
  sortGames,
  filterGames,
  loadingGames
}

export default connect(mapStateToDispatch, mapDispatchToProps)(App)
