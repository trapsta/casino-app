import { combineEpics, createEpicMiddleware } from 'redux-observable'
import gamesEpics from './gamesEpics'
import categoriesEpics from './categoriesEpics'
import searchEpics from './searchEpics'
import { IState } from '../reducers'
import { Action } from 'redux'

export const rootEpic = combineEpics(gamesEpics, categoriesEpics, searchEpics)

export default createEpicMiddleware<Action, Action, IState>()
