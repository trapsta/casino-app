import { ofType, Epic, combineEpics } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, debounceTime, switchMap, catchError } from 'rxjs/operators'
import axios from 'axios'
import {
  clearSearch,
  SearchAction,
  SearchActionTypes,
  searchGames,
  searchGamesFailed,
  searchGamesSuccess
} from '../actions/searchActions'
import { IState } from '../reducers'
import { IGameItem } from '../models'
import { urlEndpoint } from '../config'

const changeSearchEpic: Epic<SearchAction, SearchAction, IState> = (action$) =>
  action$.pipe(
    ofType(SearchActionTypes.CHANGE_SEARCH_TERM),
    map((o) => o.payload.search.trim()),
    debounceTime(100),
    map((o) => searchGames(o))
  )

const searchGamesEpic: Epic<SearchAction, SearchAction, IState> = (action$) =>
  action$.pipe(
    ofType(SearchActionTypes.SEARCH_GAMES),
    map((action) => action.payload.search),
    map((o) => (o !== '' ? o : clearSearch())),
    switchMap((search) =>
      from(axios.get(urlEndpoint)).pipe(
        map((response) =>
          searchGamesSuccess(
            response.data.filter((game: IGameItem) =>
              game.name.toLocaleLowerCase().includes(search.toLocaleLowerCase())
            )
          )
        ),
        catchError((e) => of(searchGamesFailed(e)))
      )
    )
  )

export default combineEpics(changeSearchEpic, searchGamesEpic)
