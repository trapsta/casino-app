import { Epic, ofType } from 'redux-observable'
import { switchMap } from 'rxjs/operators'
import { GamesAction, GamesActionTypes } from '../actions/gamesActions'
import { IState } from '../reducers'
import { urlEndpoint } from '../config'
import { GamesApiService } from '../ApiService'

const loadGamesEpic: Epic<GamesAction, GamesAction, IState> = (action$) =>
  action$.pipe(
    ofType(GamesActionTypes.LOAD_GAMES),
    switchMap(() => GamesApiService(urlEndpoint))
  )

// incase we have more epics we can use the combineEpics to merge them
// export default combineEpics(loadGamesEpic)
export default loadGamesEpic
