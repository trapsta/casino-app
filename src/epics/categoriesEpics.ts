import { combineEpics, Epic, ofType } from 'redux-observable'
import { switchMap } from 'rxjs/operators'
import {
  CategoriesAction,
  CategoriesActionTypes
} from '../actions/categoriesActions'
import { IState } from '../reducers'
import { categoriesUrlEndpoint } from '../config'
import { CategoriesApiService } from '../ApiService'

const loadCategoriesEpic: Epic<CategoriesAction, CategoriesAction, IState> = (
  action$
) =>
  action$.pipe(
    ofType(CategoriesActionTypes.LOAD_CATEGORIES),
    switchMap(() => CategoriesApiService(categoriesUrlEndpoint))
  )

// incase we have more epics we can use the combineEpics util function to merge them
export default combineEpics(loadCategoriesEpic)
