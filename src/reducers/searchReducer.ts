import produce from 'immer'
import { ApiStatus, IGameItem } from '../models'
import { SearchAction, SearchActionTypes } from '../actions/searchActions'

export const initialSearchState: ISearchState = {
  games: [],
  loadingStatus: ApiStatus.LOADED,
  error: null,
  search: ''
}

export default function searchReducer(
  state: ISearchState = initialSearchState,
  action: SearchAction
) {
  return produce(state, (draft) => {
    switch (action.type) {
      case SearchActionTypes.SEARCH_GAMES:
        draft.loadingStatus = ApiStatus.LOADING
        draft.error = null
        break

      case SearchActionTypes.SEARCH_GAMES_SUCESS:
        draft.loadingStatus = ApiStatus.LOADED
        draft.games = action.payload.games
        break

      case SearchActionTypes.SEARCH_GAMES_FAILED:
        draft.loadingStatus = ApiStatus.FAILED
        draft.games = []
        draft.error = action.payload.error
        break

      case SearchActionTypes.CHANGE_SEARCH_TERM:
        draft.search = action.payload.search
        break

      case SearchActionTypes.CLEAR_SEARCH:
        draft.search = ''
        draft.games = []
        draft.loadingStatus = ApiStatus.LOADED
        break
    }
  })
}

export interface ISearchState {
  readonly loadingStatus: ApiStatus
  readonly games: IGameItem[]
  readonly search: string
  readonly error: null | {}
}
