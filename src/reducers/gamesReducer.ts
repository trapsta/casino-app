import produce from 'immer'
import { ApiStatus, ICategoryItem, IGameItem, ISortCriteria } from '../models'
import { GamesAction, GamesActionTypes } from '../actions/gamesActions'

// Type-safe initialState!
export const initialGamesState: IGameState = {
  loadingStatus: ApiStatus.LOADING,
  addingStatus: ApiStatus.LOADED,
  games: [],
  sortCriteria: 'popular',
  filter: {
    id: 24,
    displayName: 'Recommended',
    visible: true,
    languageID: 5,
    tag: 'recommended',
    languageCode: 'de'
  }
}

// Thanks to Redux 4's much simpler typings, we can take away a lot of typings on the reducer side,
// everything will remain type-safe.
export default function gamesReducer(
  state: IGameState = initialGamesState,
  action: GamesAction
) {
  return produce(state, (draft) => {
    switch (action.type) {
      case GamesActionTypes.LOAD_GAMES:
      case GamesActionTypes.LOADING_GAMES:
        draft.loadingStatus = ApiStatus.LOADING
        break

      case GamesActionTypes.LOADING_GAMES_FAILED:
        draft.loadingStatus = ApiStatus.FAILED
        break

      case GamesActionTypes.LOADED_GAMES:
        draft.loadingStatus = ApiStatus.LOADED
        draft.games = action.payload.games
        break

      case GamesActionTypes.SORT_GAMES:
        draft.sortCriteria = action.payload.sortCriteria
        break

      case GamesActionTypes.FILTER_GAMES:
        draft.filter = action.payload.category
        break
    }
  })
}

// Declare state types with `readonly` modifier to get compile time immutability.
// https://github.com/piotrwitek/react-redux-typescript-guide#state-with-type-level-immutability
export interface IGameState {
  readonly loadingStatus: ApiStatus
  readonly addingStatus: ApiStatus
  readonly games: IGameItem[]
  readonly sortCriteria: ISortCriteria
  readonly filter: ICategoryItem
}
