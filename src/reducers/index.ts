import { combineReducers } from 'redux'
import gamesReducer, { IGameState, initialGamesState } from './gamesReducer'
import categoriesReducer, {
  ICategoriesState,
  initialCategoriesState
} from './categoriesReducer'
import searchReducer, {
  initialSearchState,
  ISearchState
} from './searchReducer'

// The top-level state object
export interface IState {
  games: IGameState
  categories: ICategoriesState
  search: ISearchState
}

export const initialState: IState = {
  games: initialGamesState,
  categories: initialCategoriesState,
  search: initialSearchState
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name
export default combineReducers({
  games: gamesReducer,
  categories: categoriesReducer,
  search: searchReducer
})
