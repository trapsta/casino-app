import produce from 'immer'
import { ApiStatus, ICategoryItem } from '../models'
import {
  CategoriesAction,
  CategoriesActionTypes
} from '../actions/categoriesActions'

export const initialCategoriesState: ICategoriesState = {
  loadingStatus: ApiStatus.LOADING,
  addingStatus: ApiStatus.LOADED,
  categories: []
}

export default function categoriesReducer(
  state: ICategoriesState = initialCategoriesState,
  action: CategoriesAction
) {
  return produce(state, (draft) => {
    switch (action.type) {
      case CategoriesActionTypes.LOAD_CATEGORIES:
      case CategoriesActionTypes.LOADING_CATEGORIES:
        draft.loadingStatus = ApiStatus.LOADING
        break

      case CategoriesActionTypes.LOADING_CATEGORIES_FAILED:
        draft.loadingStatus = ApiStatus.FAILED
        break

      case CategoriesActionTypes.LOADED_CATEGORIES:
        draft.loadingStatus = ApiStatus.LOADED
        draft.categories = action.payload.categories
        break
    }
  })
}

export interface ICategoriesState {
  readonly loadingStatus: ApiStatus
  readonly addingStatus: ApiStatus
  readonly categories: ICategoryItem[]
}
