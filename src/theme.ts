// use unstable_createMuiStrictModeTheme to avoid findDOMNode is deprecated in StrictMode warnings
import { unstable_createMuiStrictModeTheme as createMuiTheme } from '@material-ui/core'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#fb8f00',
      dark: '#8c92a0',
      light: '#f7f6f2'
    },
    secondary: {
      main: '#fb8f00'
    }
  }
})

export default theme
