import { ICategoryItem, IGameItem, ISortCriteria } from '../models'

// Use enums for better autocompletion of action type names. These will
// be compiled away leaving only the final value in your compiled code.
//
// Here we can use any naming conventions for your action types, but
// personally, I use the `@@context/ACTION_TYPE` convention, to follow the convention
// of Redux's `@@INIT` action.

export enum GamesActionTypes {
  LOAD_GAMES = '@@games/LOAD_GAMES',
  LOADING_GAMES = '@@games/LOADING_GAMES',
  LOADED_GAMES = '@@games/LOADED_GAMES',
  LOADING_GAMES_FAILED = '@@games/LOADING_GAMES_FAILED',
  SORT_GAMES = '@@games/SORT_GAMES',
  FILTER_GAMES = '@@games/FILTER_GAMES'
}

export function loadGames(): ILoadGamesAction {
  return {
    type: GamesActionTypes.LOAD_GAMES
  }
}

export function loadingGames(): ILoadingGamesAction {
  return {
    type: GamesActionTypes.LOADING_GAMES
  }
}

export function loadedGames(games: IGameItem[]): ILoadedGamesAction {
  return {
    type: GamesActionTypes.LOADED_GAMES,
    payload: {
      games
    }
  }
}

export function loadingGamesFailed(): ILoadingGamesFailedAction {
  return {
    type: GamesActionTypes.LOADING_GAMES_FAILED
  }
}

export function sortGames(sortCriteria: ISortCriteria): ISortGamesAction {
  return {
    type: GamesActionTypes.SORT_GAMES,
    payload: { sortCriteria }
  }
}

export function filterGames(category: ICategoryItem): IFilterGamesAction {
  return {
    type: GamesActionTypes.FILTER_GAMES,
    payload: { category }
  }
}

export interface ILoadGamesAction {
  type: GamesActionTypes.LOAD_GAMES
}

export interface ILoadingGamesAction {
  type: GamesActionTypes.LOADING_GAMES
}

export interface ILoadedGamesAction {
  type: GamesActionTypes.LOADED_GAMES
  payload: {
    games: IGameItem[]
  }
}

export interface ISortGamesAction {
  type: GamesActionTypes.SORT_GAMES
  payload: {
    sortCriteria: ISortCriteria
  }
}

export interface IFilterGamesAction {
  type: GamesActionTypes.FILTER_GAMES
  payload: {
    category: ICategoryItem
  }
}

export interface ILoadingGamesFailedAction {
  type: GamesActionTypes.LOADING_GAMES_FAILED
}

export type GamesAction =
  | ILoadGamesAction
  | ILoadingGamesAction
  | ILoadedGamesAction
  | ILoadingGamesFailedAction
  | ISortGamesAction
  | IFilterGamesAction
