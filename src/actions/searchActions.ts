import { IGameItem } from '../models'

// Use enums for better autocompletion of action type names. These will
// be compiled away leaving only the final value in your compiled code.
//
// Here we can use any naming conventions for your action types, but
// personally, I use the `@@context/ACTION_TYPE` convention, to follow the convention
// of Redux's `@@INIT` action.

export enum SearchActionTypes {
  SEARCH_GAMES = '@@search/SEARCH_GAMES',
  SEARCH_GAMES_SUCESS = '@@search/SEARCH_GAMES_SUCESS',
  SEARCH_GAMES_FAILED = '@@search/SEARCH_GAMES_FAILED',
  CHANGE_SEARCH_TERM = '@@search/CHANGE_SEARCH_TERM',
  CLEAR_SEARCH = '@@search/CLEAR_SEARCH'
}

export function searchGames(search: any): ISearchGamesAction {
  return {
    type: SearchActionTypes.SEARCH_GAMES,
    payload: { search }
  }
}

export function searchGamesSuccess(
  games: IGameItem[]
): ISearchGamesSuccessAction {
  return {
    type: SearchActionTypes.SEARCH_GAMES_SUCESS,
    payload: { games }
  }
}

export function searchGamesFailed(error: any): ISearchGamesFailedAction {
  return {
    type: SearchActionTypes.SEARCH_GAMES_FAILED,
    payload: { error }
  }
}

export function changeSearchTerm(search: any): IChangeSearchTermAction {
  return {
    type: SearchActionTypes.CHANGE_SEARCH_TERM,
    payload: { search }
  }
}

export function clearSearch(): IClearSearchAction {
  return {
    type: SearchActionTypes.CLEAR_SEARCH
  }
}

export interface ISearchGamesAction {
  type: SearchActionTypes.SEARCH_GAMES
  payload: { search: any }
}

export interface ISearchGamesSuccessAction {
  type: SearchActionTypes.SEARCH_GAMES_SUCESS
  payload: { games: IGameItem[] }
}

export interface ISearchGamesFailedAction {
  type: SearchActionTypes.SEARCH_GAMES_FAILED
  payload: { error: any }
}

export interface IChangeSearchTermAction {
  type: SearchActionTypes.CHANGE_SEARCH_TERM
  payload: { search: any }
}

export interface IClearSearchAction {
  type: SearchActionTypes.CLEAR_SEARCH
  payload?: any
}

export type SearchAction =
  | ISearchGamesAction
  | ISearchGamesSuccessAction
  | ISearchGamesFailedAction
  | IChangeSearchTermAction
  | IClearSearchAction
