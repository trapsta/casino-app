import { ICategoryItem } from '../models'

export enum CategoriesActionTypes {
  LOAD_CATEGORIES = '@@categories/LOAD_CATEGORIES',
  LOADING_CATEGORIES = '@@categories/LOADING_CATEGORIES',
  LOADED_CATEGORIES = '@@categories/LOADED_CATEGORIES',
  LOADING_CATEGORIES_FAILED = '@@categories/LOADING_CATEGORIES_FAILED'
}

export function loadCategories(): ILoadCategoriesAction {
  return {
    type: CategoriesActionTypes.LOAD_CATEGORIES
  }
}

export function loadingCategories(): ILoadingCategoriesAction {
  return {
    type: CategoriesActionTypes.LOADING_CATEGORIES
  }
}

export function loadedCategories(
  categories: ICategoryItem[]
): ILoadedCategoriesAction {
  return {
    type: CategoriesActionTypes.LOADED_CATEGORIES,
    payload: {
      categories
    }
  }
}

export function loadingCategoriesFailed(): ILoadingCategoriesFailedAction {
  return {
    type: CategoriesActionTypes.LOADING_CATEGORIES_FAILED
  }
}

export interface ILoadCategoriesAction {
  type: CategoriesActionTypes.LOAD_CATEGORIES
}

export interface ILoadingCategoriesAction {
  type: CategoriesActionTypes.LOADING_CATEGORIES
}

export interface ILoadedCategoriesAction {
  type: CategoriesActionTypes.LOADED_CATEGORIES
  payload: {
    categories: ICategoryItem[]
  }
}

export interface ILoadingCategoriesFailedAction {
  type: CategoriesActionTypes.LOADING_CATEGORIES_FAILED
}

export type CategoriesAction =
  | ILoadCategoriesAction
  | ILoadingCategoriesAction
  | ILoadedCategoriesAction
  | ILoadingCategoriesFailedAction
