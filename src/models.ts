export interface IGameItem {
  gameId: number
  vendorId: number
  name: string
  slug: string
  isNew?: boolean
  isHot?: boolean
  isLive?: boolean
  categories: { categoryId: number; orderNumber?: number }[]
  jackpot?: { gameId: number; currencyFormat: string }
}

export interface ICategoryItem {
  id: number
  displayName: string
  visible: boolean
  languageID: number
  tag: string
  languageCode: string
}

export enum ApiStatus {
  LOADING = 'loading',
  LOADED = 'loaded',
  FAILED = 'failed'
}

export type ISortCriteria = 'alphabetical' | 'popular'
