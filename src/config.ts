// Contains config variables, settings, API urls, API secrets e.t.c

// Good practice and convention to migrate some of the configs to .env file for easier updates and security
export const urlEndpoint =
  process.env.REACT_URL_ENDPOINT || '/static/data/games-list.json'

export const categoriesUrlEndpoint =
  process.env.REACT_CATEGORIES_URL_ENDPOINT ||
  '/static/data/categories-list.json'

export const imgCdn = (gameId: number) =>
  `https://img.gutsxpress.com/games/thumbs/${gameId}.jpg?width=345`

export const isMobile = () => (window.innerWidth <= 800 ? true : false)
