import { map, startWith, catchError } from 'rxjs/operators'
import { from, of } from 'rxjs'
import axios from 'axios'
import {
  loadedGames,
  loadingGames,
  loadingGamesFailed
} from './actions/gamesActions'
import {
  loadedCategories,
  loadingCategories,
  loadingCategoriesFailed
} from './actions/categoriesActions'

export const GamesApiService = (url: string) =>
  from(axios.get(url)).pipe(
    map((response) => loadedGames(response.data)),
    startWith(loadingGames()),
    catchError(() => of(loadingGamesFailed()))
  )

export const CategoriesApiService = (url: string) =>
  from(axios.get(url)).pipe(
    map((response) => loadedCategories(response.data)),
    startWith(loadingCategories()),
    catchError(() => of(loadingCategoriesFailed()))
  )
