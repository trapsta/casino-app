# Casino App Documentation

## Live app url - [https://casino-app.web.app](https://casino-app.web.app)

## Background
Casino App is the online gaming app for an award winning sports betting company for their users who want to play online.

## Assumptions

 - The app will be used by multiple users at once hence the need to be robust, scalable and support concurrency.
 - Users expect a snappy and responsive UI hence need to optimise loading resources and computations.
- Some users might be using the app on mobile devices so the UI needs to responsive and accommodate different screen size.
- We will use the Container-Component pattern to structure the app where containers will be connected to the store and components will contain the presentantion logic.
-   Pages/Screens will be unique parts of our application that will group the main container component and all non-reusable presentational components.
-   Components: All other reusable components, like buttons, cards, Form elements

## Tech Stack
Casino App is built using a modern technology stack that includes:

 1. **CRA-Webpack** - Module Bundler
 2. **ReactJs** - View Layer
 3. **TypeScript** - Strong Static Typing
 4. **Redux** - State Management
 5. **Redux-Observable** - Redux Middleware
 6. **Reselect** - Memoized State Selector
 7. **Jest** - Test Runner
 8. **React Testing-library** - Component Testing
 9. **Material-UI** - UI Framework


## Quickstart Installation

1. Make sure that you have Node.js v8.15.1 and npm v5 or above installed.

2. Clone this repo using `git clone https://gitlab.com/trapsta/casino-app.git`

3. CD into the appropriate directory: `cd casino-app`.

4. Run `yarn install` in order to install dependencies .

5. Run `yarn start` to see the development server at `http://localhost:3000` .


## File Structure

```
casino-app
│   README.md
└─  src
	   │   __ tests __
	   │   components
	   │   containers
	   │   epics
	   │   reducers
	   │   selectors
	   │   ApiService
	   │   models
	   │   store
	   │   index

```

## Challenges

 1. App Architecture - choosing the architecture, patterns , file structure was challenging since this was an isolated part of a possibly larger app or ecosystems of app and there were also lots of different ways to design the implementation such as MVC , MVVM, VIPER.  In the end I went with the components based architecture because it's simple, flexible and scalable.

2. Optimizing the loading of the cards - Loading the cards would need to be optimized since loading so many components at once in the dom for categories that had a lot of games could cause issues and make the UI laggy or unresponsive.  The best way to do this would have been using some windowing techniques such as lazy loading using a library such as React-Window, caching the games images, memoization e.t.c Some the solutions were quite beyond the scope of the project but in the end having an eagle eye on where needed to be optimized was the most important part.

4. Implementing sorting and filters - It was hard to decide where exactly to implement sorting and filters since I didn't want to put this logic in the components since this is not presentational logic, I also did not want to store the filtered state in the store since this would either cause duplicated data or I would need to refetch the data from the api every time the user needed to filter by a new category. In the end, I decided to save the sort key and the filter category in the store and use those to compute the filtered/sorted data in the selectors which I felt was a more elegant solution since I wouldn't need to duplicate the state or refetch the data again. 